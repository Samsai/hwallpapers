# hwallpapers, simple randomized wallpaper changer

## About

hwallpapers is a simple randomized wallpaper changer for Linux desktop environments that
allow a wallpaper to be set using the "feh" image viewer. It is usable, for example,
with i3wm and similar window managers without their own wallpaper utilities.

## Installation

hwallpapers is written in Haskell and can be built from the source code directory
with "stack build" and installed with "stack install". The "feh" utility must also be
installed for the program to function. You can autostart hwallpapers from your
~/.config/i3/config or a similar autostart script of a config file.

## Usage

hwallpapers is launched by giving it at least two parameters. The first command line
argument is a time in seconds which the program will wait before changing to the next
wallpaper. All the rest of the arguments are paths to images that are used as your
wallpapers. The program then shuffles all of these files and loops through the
shuffled list, passing each file to "feh" and then waiting the designated amount of
time.
