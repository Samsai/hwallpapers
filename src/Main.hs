module Main where

import System.Process
import System.Environment
import System.Random
import Data.Traversable
import Data.Array.IO
import Data.List
import Control.Monad
import Control.Concurrent


randomSequence :: Int -> StdGen -> [Int]
randomSequence length gen
    | length == 0 = []
    | length > 0 = randomSequence' length gen []
    
randomSequence' 0 gen nums = []
randomSequence' length gen nums =
    let (next, nextGen) = random gen
    in  if not (any (\x -> x == next) nums)
        then next : (randomSequence' (length - 1) nextGen (next : nums))
        else randomSequence' length nextGen nums

myShuffle2 :: Int -> [a] -> [a]
myShuffle2 seed items =
    let gen = mkStdGen seed
        nums = randomSequence (length items) gen
        pairs = zip nums items
    in map (\x -> case lookup x pairs of
                        Just a -> a
                        Nothing -> (head items)
           ) (sort nums)

runCommandDelay :: String -> Int -> IO ()
runCommandDelay cmd delay = do
   a <- runCommand cmd
   waitForProcess a
   threadDelay (delay * 1000 * 1000)
   return ()

main :: IO ()
main = do
    args <- getArgs 
    putStrLn "Haskell Wallpaper Switcher"
    let delay = read (head args) :: Int
     
    seed <- randomIO
    
    let files = myShuffle2 seed (tail args)
    forever  $ forM files (\x -> runCommandDelay ("feh --bg-scale  \"" ++ x ++ "\"") delay)
    return ()
